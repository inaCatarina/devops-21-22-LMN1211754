# CA4/Part2 - Container Practice                  

## Using Docker Compose to run multi container applications                

### By Catarina Nogueira                   

*Commands are run using **Git Bash Here** or **Windows Power-Shell**
except if another tool is mentioned*                                                    

**Step1:** Use **Git Bash Here** in CA4 folder                                                                                                                                                        
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4                                   

**Step2:** Create part2 folder in CA4 folder                                  
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4                            
- mkdir part2                                

**Step3:** Create readme file                                
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part2                                    
- touch readme.md                   

### Point2                 
**Step4:** Create docker-compose.yml file               
- created docker-compose.yml file via intelliJ                   

### Point2                      
**Step5:** Use **Git Bash Here** in CA4/part2 folder to create db and web folders                            
- mkdir db                        
- mkdir web                      

### Point2            
**Step6:** Create Dockerfile in db folder              
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part2/db                  
- touch Dockerfile                        

### Point2             
**Step7:** Create Dockerfile in web folder              
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part2/web                  
- touch Dockerfile                            

**Step8:** Use **Git Bash Here** in CA4/part2 folder to create a data folder for the db backup                 
- mkdir data               

### Point1             
**Step9:** Copy the react-and-spring-data-rest-basic from CA3/part2 application into CA4/part2 folder                    
*using **Git Bash Here** in CA4/part2 folder                        
- cp /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA3/part2 /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part2                    

### Point2           
**Step10:** Edited Dockerfile in CA4/part2/web               
*made the necessary changes on the file for my application*                    
- see Dockerfile                          
- used tomcat version 9.0-jdk11-temurin since it is compatible with jdk11 and our spring application version 2.6.6                        
           
### Point2              
**Step11:** Edited Dockerfile in CA4/part2/db                       
*made the necessary changes on the file for my application*                                 
- see Dockerfile                
- used ubuntu:18.04 since it is compatible with jdk11                        

### Point2               
**Step12:** Edited docker-compose.yml in CA4/part2              
*made the necessary changes on the file for my application*                  
- see docker-compose.yml              

**Step13:** Committing files to repository                 
- git add .              
- git commit -m "committing files and updating readme addresses #41"              
- git push            

### Point2            
**Step14:** Starting and running the docker images from docker-compose.yml            
*using **Git Bash Here** in CA4/part2 folder               
- docker-compose up                      

**Step15:** Had to change the .gitignore file in order for the gradle-wrapper.jar and react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war files                    
to be pushed to the remote repository                 
*I also forced the commit*             
- git add -f gradle-wrapper.jar                
- git commit -m "commiting gradle-wrapper.jar"               
- git push                 

- git add -f react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war            
- git commit -m "react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war"                    
- git push              

**Step16:** Testing the web and db container:                  
*web container*                  
- run http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/                  
- working as intended              
- ![](annex1.PNG)                     

*db container*                     
- run http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console                    
- For the connection string use: jdbc:h2:tcp://192.168.42.11:9092/./jpadb             
- working as intended             
- ![](annex2.PNG)                   

### Point3                    
**Step17:** Tagging the web image and publishing it on Docker Hub                                
*using **Git Bash Here** in CA4/part2 folder               
- docker login            
- docker tag part2_web inacatarina/part2_web:1.0        
- docker push inacatarina/part2_web:1.0                      

### Point3                 
**Step18:** Tagging the db image and publishing it on Docker Hub                                    
*using **Git Bash Here** in CA4/part2 folder                
- docker login            
- docker tag part2_db inacatarina/part2_db:1.0               
- docker push inacatarina/part2_db:1.0                    

### Point4                     
**Step19:** Copy the database file from the container to the shared folder in the host machine               
- *using **Windows Power-Shell** in CA4/part2 folder               
- docker-compose up (if the containers aren't running)          
- docker-compose exec db bash                   
- pwd              
- ls -la              
- cp jpadb.mv /usr/src/data-backup
*verified that the file jpadb.mv.db is also on my host-machine in the data folder in CA4/part2*

##Point5                  
**Step20:** Committing readme to repository             
- git add .               
- git commit -m "commiting updated readme"          
- git push      
- git tag ca4-part2                       
- git push origin ca4-part2                            
   








