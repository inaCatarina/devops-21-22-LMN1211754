# CA4/Part1A - Container Practice                      

## Using DockerFile with DockerDesktop                   

## Creating DockerFile to run server                      

### By Catarina Nogueira                   

*Commands are run using **Git Bash Here** or **Windows Power-Shell** 
except if another tool is mentioned*                              

**Step1:** Use **Git Bash Here** in folder                                                                                                            
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754                        

**Step2:** Create CA4 folder                     
- mkdir CA4                        

**Step3:** Create part1A folder in CA4 folder                
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4                   
- mkdir part1A                   

**Step4:** Create readme file                
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA4/part1A                                
- touch readme.md                    

**Step5:** Install WSL 2                 
*Because I have Windows 10 Home Edition I had to install WSL 2 in order to be able to install              
and run Docker Desktop*               
-wsl --install -d Ubuntu-20.04 (run using **Windows Power-Shell**)            
-for more instructions see https://docs.microsoft.com/en-us/windows/wsl/install           
- also see https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi                

**Step6:** Install Docker Desktop             
- see https://docs.docker.com/desktop/windows/install/                     

**Step7:** Setup a docker file                  
- create a txt file in intelliJ        
- see Dockerfile in part1A                                

**Step8:** To create an image                    
*using **Git Bash Here** in CA4/part1A folder               
- docker build -t ca4part1a .                         

**Step9:** Create an account on Docker Hub                  

**Step10:** Create and run a container                  
*using **Git Bash Here** in CA4/part1A folder       
- docker run --name container_1 -p 59001:59001 -d ca4part1a                

*here I'm naming my container by --name command and then with -d I'm calling my image*                 

**Step11:** Tagging the image and publishing it on Docker Hub                 
*using **Windows Power-Shell** in CA4/part1A folder                        
- docker login                      
- docker tag ca4part1a inacatarina/ca4part1a:1.0              
- docker push inacatarina/ca4part1a:1.0                       

*This is the equivalent to:*                
- docker tag image user/image:tag_version                  
- docker push user/image:tag_version                   

*checked Docker Desktop and my image was pushed to my remote repository*                 

**Step12:** Execute the chat client on the host machine                 
*using **Git Bash Here** in DEVOPS/gradle_basic_demo                                
(I had previously cloned repo from git clone https://Catarina_Nogueira@bitbucket.org/luisnogueira/gradle_basic_demo.git)*                 
- ./gradlew runClient                

*the chat application is working as intended*                               

**Step13:** Committing files to repository                                     
- git add .                               
- git commit -m "committing files and updating readme resolves #39"                           
- git push                    
- git tag ca4-part1a                    
- git push origin ca4-part1a                                      





