# CA5/Part1 - Jenkins Practice          

## Creating a Pipeline            

### By Catarina Nogueira                    

*Commands are run using **Git Bash Here** or **Windows Power-Shell**
except if another tool is mentioned*                     

**Step1:** Use **Git Bash Here** in folder                                                                                                                           
/c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754                   

**Step2:** Create CA5 folder                  
- mkdir CA5                  

**Step3:** Create part1 folder in CA5 folder                
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA5               
- mkdir part1                   

**Step4:** Create readme file                   
- cd /c/Users/Ina/Documents/Switch/DEVOPS/devops-21-22-lmn-1211754/CA5/part1               
- touch readme.md                

**Step5:** Install Jenkins           
- download jenkins.war (use link: https://www.jenkins.io/download/)                 
*using **Git Bash Here** in downloads folder:*                    
- java -jar jenkins.war                  

*because the port8080 was already in use had to use a different command to             
run jenkins*                      
- java -jar jenkins.war --httpPort=9090                   
*this command allows to install/run jenkins while setting a specific port at the same time*                                    

**Step6:** Copy the application gradle_basic_demo from CA2/part1 to CA5/part1                    

## Point 2           
**Step7:** Setting a pipeline script via Ide                 
*using intelliJ*               
- create a Jenkinsfile in CA5/part1                
- defining the necessary stages and post within the file                  

## Point 1             
**Step8:** Setting new pipeline job                
- use http://localhost:9090/             
- select *new item*             
- select *pipeline*                  
- name the job (I have named it as ca5part1)           
- choose the option *Pipeline script from SCM*                  
- choose Git as SCM                   
- place repository URL https://bitbucket.org/catarina_nogueira/devops-21-22-lmn-1211754                      
- specify branch as */main                    
- specify script path to CA5/part1/gradle_basic_demo/Jenkinsfile                 

**Step9:** Had to change the .gitignore file in order for the gradle-wrapper.jar and build folder to be pushed to the remote repository                                     
*I also forced the commit*              
- git add -f gradle-wrapper.jar              
- git commit -m "commiting gradle-wrapper.jar"           
- git push                    
- git add -f build               
- git commit -m "commiting build folder"     
- git push                    

**Step10:**                 
- on http://localhost:9090/            
- choose job ca5part1          
- choose build now         
- working as intended                           

## Point3              
**Step11:** Committing readme to repository                    
- git add .            
- git commit -m "commiting updated readme"              
- git push          
- git tag ca5-part1              
- git push origin ca5-part1                                 


