# CA2/Part1 - Gradle Practice

## Build tools with Gradle, a Tutorial

### By Catarina Nogueira

*Commands are run using **Git Bash Here** except if another tool is mentioned*

**Step1:** Use **Git Bash Here** in folder /d/Switch/devops-21-22-lmn-1211754

**Step2:** Create CA2 folder
- mkdir CA2

**Step3:** Create folder part1
- cd /d/Switch/devops-21-22-lmn-1211754/CA2
- mkdir part1

**Step4:** Create the readme.md file
- cd /d/Switch/devops-21-22-lmn-1211754/CA2/part1
- touch readme.md

**Step5:** Clone the gradle_basic_demo repository
- git clone https://Catarina_Nogueira@bitbucket.org/luisnogueira/gradle_basic_demo.git

**Step6:** Remove the .git folder
- rm -rf .git

**Step7:** Commit the changes to the repository:
- git add .
- git commit -m "Creating dir CA2 and part1. Cloning gradle basic demo into dir part1. (resolves #7, #8, #9, #10)"
- git push

*Followed the instructions on the repository to experiment with the application:      
https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/README.md*

**Step8:** Build a .jar file
- cd /d/Switch/devops-21-22-lmn-1211754/CA2/part1/gradle_basic_demo
- ./gradlew build

**Step9:** Run the server
- open a terminal and execute the following command
- java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

**Step10:** Run a client
- open another terminal to run the following command
- ./gradlew runClient

**Step11:** Open an IDE (choose IntelliJ) to make changes to the build.gradle file

**Step12:** Added a new task to execute the server
- created new task runServer in build.gradle file

**Step13:** Added a JUnit dependency 
- JUnit 4.12 dependency added ti build.gradle file

**Step14:** Generated a test class
- created class AppTest.java
- run in the command line:
  - ./gradlew test

**Step15:** Commit changes to repository
- git add .
- git commit -m "created new task in build.gradle. Added JUnit 4.12 dependency to gradle. Created class AppTest.java (resolves #11, #12, #13)"
- git push

**Step16:** Create new task to copy the source folder
- create new task copySrc in build.gradle file
- run in the command line:
  - ./gradlew copySrc

**Step17:** Commit changes to repository
- git add .
- git commit -m "created new task copySrc. (resolves #14)"
- git push

**Step18:** Create new task zip to generate a zip file
- create new task zipArchive in build.gradle file
- run in the command line:
  - ./gradlew zipArchive

**Step19:** Commit changes to repository
- git add .
- git commit -m "created new zip task. (resolves #15)"
- git push

**Step20:** Completed readme tutorial, committing changes to the repository, adding ca2-part1
- git add .
- git commit -m "committing completed readme. (resolves #16)"
- git push
- git tag ca2-part1
- git push origin ca2-part1

